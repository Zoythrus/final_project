import 'package:flutter/material.dart';
import 'package:sightseer/main.dart' as main;
import 'package:sightseer/forumpost.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:google_sign_in/google_sign_in.dart';

final googleSignIn = new GoogleSignIn();
final analytics = new FirebaseAnalytics();
//final auth = FirebaseAuth.instance;
var currentUserEmail;

class ForumHome extends StatefulWidget {
  @override
  ForumHomeState createState() {
    return new ForumHomeState();
  }
}

class ForumHomeState extends State<ForumHome> {
  final List<Widget> _posts = <Widget>[];

  List<ForumPostList> forumMessages = List();
  ForumPostList forumItem;

  final reference = FirebaseDatabase.instance.reference().child('forum');

  @override
  void initState() {
    super.initState();

    forumItem = ForumPostList();
    reference.onChildAdded.listen(_onEntryAddedMessage);
    reference.onChildRemoved.listen(_onEntryRemovedMessage);
    reference.onChildChanged.listen(_onEntryChangedMessage);
    //reference.keepSynced(true);

    _ensureLoggedIn();
  }

  _onEntryAddedMessage(Event event) {
    //setState(() {
    forumMessages.add(ForumPostList.fromSnapshot(event.snapshot));
    //});
  }

  _onEntryRemovedMessage(Event event) {
    //setState(() {
    forumMessages.remove(ForumPostList.fromSnapshot(event.snapshot));
    //});
  }

  _onEntryChangedMessage(Event event) {
    var old = forumMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    //setState(() {
    forumMessages[forumMessages.indexOf(old)] =
        ForumPostList.fromSnapshot(event.snapshot);
    //});
  }

  Widget renderAppBar() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Color.fromRGBO(255, 253, 208, 100),
      title: Text(
        "Forum",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
            length: 1,
            child: Scaffold(
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  backgroundColor: Color.fromRGBO(102, 0, 0, 50),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => ForumPost()),
                    );
                  },
                ),
                backgroundColor: Color.fromRGBO(102, 0, 0, 50),
                appBar: renderAppBar(),
                body: Container(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Flexible(
                      child: new FirebaseAnimatedList(
                        query: reference.orderByChild("location").equalTo(
                            main.MapFunctionality().getInitialPlace.locality),
                        padding: const EdgeInsets.all(8.0),
                        defaultChild: Container(child: Text("Loading....")),
                        reverse: false,
                        sort: (a, b) =>
                            b.value['upvotes'].compareTo(a.value['upvotes']),
                        itemBuilder: (_, DataSnapshot snapshot,
                            Animation<double> animation, int x) {
                          if (snapshot.value['imageUrl'] != null) {
                            return new Card(
                              color: Color.fromRGBO(255, 253, 208, 1),
                              child: Column(children: <Widget>[
                                ListTile(
                                  title: new Text(snapshot.value['senderName']),
                                  subtitle: new Text(snapshot.value['text']),
                                  leading: new Image.network(
                                      snapshot.value['imageUrl']),
                                ),
                                ButtonTheme.bar(
                                  child: ButtonBar(
                                    children: <Widget>[
                                      FlatButton(
                                        color: Color.fromRGBO(102, 0, 0, 50),
                                        child: new Text(snapshot
                                            .value['upvotes']
                                            .toString()),
                                        onPressed: () {

                                          reference
                                              .child(snapshot.key)
                                              .child('upvotes')
                                              .set(snapshot.value['upvotes'] +
                                                  1);

                                          reference
                                              .child(snapshot.key)
                                              .child('liked')
                                              .push()
                                          .set(googleSignIn.currentUser.email);

                                          analytics.logEvent(
                                              name: 'send_message');
                                        },
                                      )
                                    ],
                                  ),
                                )
                              ]),
                            );
                          } else {
                            return new Card(
                              color: Color.fromRGBO(255, 253, 208, 1),
                              child: Column(children: <Widget>[
                                ListTile(
                                  title: new Text(snapshot.value['senderName']),
                                  subtitle: new Text(snapshot.value['text']),
                                ),
                                ButtonTheme.bar(
                                  child: ButtonBar(
                                    children: <Widget>[
                                      FlatButton(
                                        color: Color.fromRGBO(102, 0, 0, 50),
                                        child: new Text(snapshot
                                            .value['upvotes']
                                            .toString()),
                                        onPressed: () {

                                            print(reference.child('liked').equalTo(googleSignIn.currentUser.email));
                                            reference
                                                .child(snapshot.key)
                                                .child('upvotes')
                                                .set(snapshot.value['upvotes'] +
                                                1);

                                            reference
                                                .child(snapshot.key)
                                                .child('liked')
                                                .push()
                                                .set(
                                                googleSignIn.currentUser.email);

                                            analytics.logEvent(
                                                name: 'send_message');

                                        },
                                      )
                                    ],
                                  ),
                                )
                              ]),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                )))));
  }

  Future<Null> _ensureLoggedIn() async {
    GoogleSignInAccount signedInUser = googleSignIn.currentUser;

    if (signedInUser == null)
      signedInUser = await googleSignIn.signInSilently();

    if (signedInUser == null) {
      await googleSignIn.signIn();

      analytics.logLogin();
    }

    //await auth.signInWithGoogle(
    //  idToken: credentials.idToken, accessToken: credentials.accessToken);
  }
}

class ForumPostList {
  String key;
  String text;
  String email;
  var upvotes;
  List<String> liked;
  var imageURL;
  var senderPhotoURL;
  String name;
  String location;

  ForumPostList(
      {this.key,
      this.text,
      this.email,
      this.upvotes,
      this.liked,
      this.imageURL,
      this.senderPhotoURL,
      this.name,
      this.location});

  ForumPostList.fromSnapshot(DataSnapshot snapshot) {
    key = snapshot.key;
    text = snapshot.value["messageText"];
    email = googleSignIn.currentUser.email;
    imageURL = snapshot.value["imageUrl"];
    name = googleSignIn.currentUser.displayName;
    senderPhotoURL = googleSignIn.currentUser.photoUrl;
    location = snapshot.value["location"];
    upvotes = snapshot.value["upvotes"];
    liked = null;
  }
}
