import 'package:flutter/material.dart';
import 'package:sightseer/homepage.dart';
import 'package:sightseer/chatpage.dart';
import 'package:sightseer/mappage.dart';
import 'package:sightseer/forum.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sightseer/main.dart' as main;
import 'package:sightseer/forumpost.dart';
//import 'package:firebase_integration/Setup/signin.dart'

class SecondMain extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Sightseer',
      home: PageView(
        children: <Widget>[
          ForumHome(),
          //ForumPost(),
          ChatPage(),
          WebMapPage(),
        ],
      ),
    );
  }
}

