import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sightseer/main.dart' as main;
//import 'package:sightseer/secondmain.dart' as secondmain;

class WebMapPage extends StatefulWidget {

  @override
  WebMap createState() => new WebMap();
}

class WebMap extends State<WebMapPage> {


  GoogleMapController mapController;
  var map = main.MapFunctionality();

  void initState() {
    // TODO: implement initState
    super.initState();

    setState(() {    });

  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar:

      AppBar(
        title:
        map.getInitialPlace == null
            ? Text("Unknown", textAlign: TextAlign.center)
            :
        Text(map.getInitialPlace.locality, textAlign: TextAlign.center),
        backgroundColor: Color.fromRGBO(255, 253, 208, 100),
      ),

      body:
      map.getLocation == null
          ? CircularProgressIndicator(backgroundColor: Colors.blueGrey)
          :
      Container(
        color: Color.fromRGBO(102, 0, 0, 50),
        child:  Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(10),
                height:500,
                width: MediaQuery.of(context).size.width,
                //color: Color.fromRGBO(102, 0, 0, 100),
                child: GoogleMap(
                  onMapCreated: (GoogleMapController _mapController) {
                    mapController = _mapController;
                  },
                  scrollGesturesEnabled: true,
                  //myLocationEnabled: true,
                  rotateGesturesEnabled: true,
                  zoomGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  initialCameraPosition: new CameraPosition(
                      zoom: 13.0,
                      target: LatLng(
                          map.getInitialPosition.latitude, map.getInitialPosition.longitude)
                  ),
                )
            )
          ],
        ),
      ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color.fromRGBO(255, 253, 208, 100),
          foregroundColor: Color.fromRGBO(102, 0, 0, 100),
          onPressed: () {
          map.getLocation().then((value) {
            map.setCurrentLocation  =value;
            print(value.altitude);
            mapController.animateCamera(
                CameraUpdate.newCameraPosition(
                    CameraPosition(
                        target: LatLng(value.latitude, value.longitude),
                        zoom: 13.0
                    )
                )
            );
            map.getAddress(value).then((temp) {
              map.setCurrentAddress = temp;
              print(temp.locality);
            });
          });
        },
          child: const Icon(Icons.my_location, size: 36.0),
        ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

