import 'package:flutter/material.dart';
import 'package:sightseer/Setup/signin.dart';
import 'package:geolocator/geolocator.dart';
//import 'package:firebase_integration/Setup/signin.dart'

void main() => runApp(MyApp());


const kGoogleApiKey = "AIzaSyDO3Z02-fkDqQd8ZrzLAcElJsRl6e5v5NQ";


class MyApp extends StatefulWidget {
  @override
  Main createState() => Main();
}

class Main extends State<MyApp> {

  var map = new  MapFunctionality();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    MapFunctionality().getLocation().then((value) {
      //print(value.altitude);

      map.setCurrentLocation = value;
      print(map.getInitialPosition);

      MapFunctionality().getAddress(value).then((temp) {
        map.setCurrentAddress = temp;
        print(map.getInitialPlace.locality);
      });
      setState(() {});

    });
  }


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Sightseer',
      home:
      MapFunctionality().getLocation() == null
          ? Container(
        width:100,
        height: 500,
        color: Colors.amber,
      )
          :
      PageView(
        children: <Widget>[
          LoginPage()
        ],
      ),
    );
  }

}

class MapFunctionality{

  static Placemark _initialAddress;
  static Position _initialLocation;

  Future<Position> getLocation() async {
    Position temp = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    return temp;
  }

  Future<Placemark> getAddress(location) async {
    List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(
        _initialLocation.latitude, _initialLocation.longitude);
    var tempAddress = placemark[0];
    return tempAddress;
  }

  Position get getInitialPosition {
    return _initialLocation;
  }

  Placemark get getInitialPlace {
    return _initialAddress;
  }

  set setCurrentLocation(Position location){
    _initialLocation = location;
  }

  set setCurrentAddress(Placemark address){
    _initialAddress = address;
  }

}