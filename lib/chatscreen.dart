import 'dart:async';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sightseer/main.dart' as main;

final googleSignIn = new GoogleSignIn();
final analytics = new FirebaseAnalytics();
final auth = FirebaseAuth.instance;
var currentUserEmail;
var _scaffoldContext;

class ChatScreen extends StatefulWidget {
  @override
  ChatScreenState createState() {
    return new ChatScreenState();
  }
}

class ChatMessageListItem {
  String key;
  String text;
  String email;

  var imageURL;
  var senderPhotoURL;
  String name;
  String location;

  ChatMessageListItem(
      {this.key,
        this.text,
        this.email,
        this.imageURL,
        this.senderPhotoURL,
        this.name,
        this.location});

  ChatMessageListItem.fromSnapshot(DataSnapshot snapshot) {
    key = snapshot.key;
    text = snapshot.value["messageText"];
    email = googleSignIn.currentUser.email;
    imageURL = snapshot.value["imageUrl"];
    name = googleSignIn.currentUser.displayName;
    senderPhotoURL = googleSignIn.currentUser.photoUrl;
    location = snapshot.value["location"];
  }
}

class ChatScreenState extends State<ChatScreen> {
  final TextEditingController _textEditingController =
  new TextEditingController();

  bool _isComposingMessage = false;

  List<ChatMessageListItem> chatMessages = List();
  ChatMessageListItem chatItem;

  final reference = FirebaseDatabase.instance.reference().child('messages');

  @override
  void initState() {
    super.initState();

    chatItem = ChatMessageListItem();
    reference.onChildAdded.listen(_onEntryAddedMessage);
    reference.onChildRemoved.listen(_onEntryRemovedMessage);
    reference.onChildChanged.listen(_onEntryChangedMessage);
    //reference.keepSynced(true);

    _ensureLoggedIn();
  }

  _onEntryAddedMessage(Event event) {
    //setState(() {
      chatMessages.add(ChatMessageListItem.fromSnapshot(event.snapshot));
    //});
  }

  _onEntryRemovedMessage(Event event) {
    //setState(() {
      chatMessages.remove(ChatMessageListItem.fromSnapshot(event.snapshot));
    //});
  }

  _onEntryChangedMessage(Event event) {
    var old = chatMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    //setState(() {
      chatMessages[chatMessages.indexOf(old)] =
          ChatMessageListItem.fromSnapshot(event.snapshot);
    //});
  }

  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Color.fromRGBO(102, 0, 0, 50),
        body: new Container(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: new FirebaseAnimatedList(
                  query: reference.orderByChild("location").equalTo(
                      main.MapFunctionality().getInitialPlace.locality),

                  padding: const EdgeInsets.all(8.0),
                  defaultChild: Container(child: Text("Loading....")),
                  reverse: true,
                  sort: (a, b) => b.key.compareTo(a.key),
                  itemBuilder: (_, DataSnapshot snapshot,
                      Animation<double> animation, int x) {
                    if (snapshot.value['text'] != null){
                      if (snapshot.value['email'] ==
                          googleSignIn.currentUser.email) {
                        return new Card(
                          color: Color.fromRGBO(255, 253, 208, 1),
                          child: ListTile(
                            title: new Text(snapshot.value['senderName']),
                            subtitle: new Text(snapshot.value['text']),
                            leading: new Image.network(
                                snapshot.value['senderPhotoUrl']),
                          ),
                          //: animation,
                        );
                      } else {
                        return new Card(
                          color: Color.fromRGBO(255, 253, 208, 100),
                          child: ListTile(
                            title: new Text(snapshot.value['senderName']),
                            subtitle: new Text(snapshot.value['text']),
                            trailing: new Image.network(
                                snapshot.value['senderPhotoUrl']),
                          ),
                          //: animation,
                        );
                      }
                    }
                    else{
                      if (snapshot.value['email'] ==
                          googleSignIn.currentUser.email) {
                        return new Card(
                          color: Color.fromRGBO(255, 253, 208, 1),
                          child: ListTile(
                            title: new Text(snapshot.value['senderName']),
                            subtitle: new Image.network(
                                snapshot.value['imageUrl']),
                            leading: new Image.network(
                                snapshot.value['senderPhotoUrl']),
                          ),
                          //: animation,
                        );
                      } else {
                        return new Card(
                          color: Color.fromRGBO(255, 253, 208, 100),
                          child: ListTile(
                            title: new Text(snapshot.value['senderName']),
                            subtitle: new Image.network(
                                snapshot.value['imageUrl']),
                            trailing: new Image.network(
                                snapshot.value['senderPhotoUrl']),
                          ),
                          //: animation,
                        );
                      }
                    }
                  },
                ),
              ),
              new Divider(height: 1.0),
              new Container(
                decoration:
                new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
              new Builder(builder: (BuildContext context) {
                _scaffoldContext = context;

                return new Container(width: 0.0, height: 0.0);
              })
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
              border: new Border(
                  top: new BorderSide(
                    color: Colors.grey[200],
                  )))
              : null,
        ));
  }

  CupertinoButton getIOSSendButton() {
    return new CupertinoButton(
      child: new Text("Send"),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  IconButton getDefaultSendButton() {
    return new IconButton(
      icon: new Icon(Icons.send),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  Widget _buildTextComposer() {
    return new IconTheme(
        data: new IconThemeData(
          color: _isComposingMessage
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: new IconButton(
                    icon: new Icon(
                      Icons.photo_camera,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: () async {
                      await _ensureLoggedIn();

                      File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

                      int timestamp = new DateTime.now().millisecondsSinceEpoch;

                      StorageReference storageReference = FirebaseStorage
                          .instance
                          .ref()
                          .child("img_" + timestamp.toString() + ".jpg");

                      StorageUploadTask uploadTask =
                      storageReference.putFile(imageFile);

                      String location =
                          main.MapFunctionality().getInitialPlace.locality;
                      //print(location);

                      StorageTaskSnapshot storage =
                          await uploadTask.onComplete;

                      String downloadUrl = await storage.ref.getDownloadURL();

                      _sendMessage(
                          messageText: null,
                          imageUrl: downloadUrl.toString(),
                          location: location);
                    }),
              ),
              new Flexible(
                child: new TextField(
                  autocorrect: true,
                  controller: _textEditingController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isComposingMessage = messageText.length > 0;
                    });
                  },
                  onSubmitted: _textMessageSubmitted,
                  decoration:
                  new InputDecoration.collapsed(hintText: "Send a message"),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? getIOSSendButton()
                    : getDefaultSendButton(),
              ),
            ],
          ),
        ));
  }

  Future<Null> _textMessageSubmitted(String text) async {
    _textEditingController.clear();

    setState(() {
      _isComposingMessage = false;
    });

    await _ensureLoggedIn();

    _sendMessage(
        messageText: text,
        imageUrl: null,
        location: main.MapFunctionality().getInitialPlace.locality);
  }

  void _sendMessage({String messageText, String imageUrl, String location}) {
    reference.push().set({
      'text': messageText,
      'email': googleSignIn.currentUser.email,
      'imageUrl': imageUrl,
      'senderName': googleSignIn.currentUser.displayName,
      'senderPhotoUrl': googleSignIn.currentUser.photoUrl,
      'location': location,
    });

    analytics.logEvent(name: 'send_message');
  }

  Future<Null> _ensureLoggedIn() async {
    GoogleSignInAccount signedInUser = googleSignIn.currentUser;

    if (signedInUser == null)
      signedInUser = await googleSignIn.signInSilently();

    if (signedInUser == null) {
      await googleSignIn.signIn();

      analytics.logLogin();
    }

    currentUserEmail = googleSignIn.currentUser.email;

    if (await auth.currentUser() == null) {
      GoogleSignInAuthentication credentials =
      await googleSignIn.currentUser.authentication;

      //await auth.signInWithGoogle(
      //  idToken: credentials.idToken, accessToken: credentials.accessToken);
    }
  }
}
