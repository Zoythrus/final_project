import 'package:flutter/material.dart';
import 'main.dart' as main;

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return new Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(255, 253, 208, 100),
          title: Text("HomePage"),
        ),
        body: Container(
          color: Color.fromRGBO(102, 0, 0, 100),
          child: Center(
            child: Text('This is the Homepage Swipe Left'),
          ),
        ),
      );
    }
  }
