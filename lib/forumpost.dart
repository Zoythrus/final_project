import 'package:flutter/material.dart';
import 'package:sightseer/main.dart' as main;
import 'dart:async';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ForumPost extends StatefulWidget {
  @override
  _ForumPostState createState() => new _ForumPostState();
}

class _ForumPostState extends State<ForumPost> {
  final googleSignIn = new GoogleSignIn();
  final analytics = new FirebaseAnalytics();
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isComposingMessage = false;
  final reference = FirebaseDatabase.instance.reference().child('forum');
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    var _imageFile;

    /*_imageSelectorGallery() async {

      //print("You chose: " + file.path);
      setState(() {
        _imageFile = ImagePicker.pickImage(source: ImageSource.gallery);

      });

      //print(imageFile);
    }*/

    return new Scaffold(
        backgroundColor: Color.fromRGBO(255, 253, 208, 100),
        appBar: new AppBar(
          title: new Text("Add New Trend"),
          backgroundColor: Color.fromRGBO(102, 0, 0, 50),
        ),
        body: new Builder(builder: (BuildContext context) {
          return new Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new Padding(padding: new EdgeInsets.only(top: 20.0),),
              new TextField(
                autofocus: true,
                autocorrect: true,
                maxLines: 7,
                controller: _textEditingController,
                onChanged: (String messageText) {
                  setState(() {
                    _isComposingMessage = messageText.length > 0;
                  });
                },
                //onSubmitted: _textMessageSubmitted,
                decoration: new InputDecoration(
                    hintText: "Send a message",
                    fillColor: Colors.white,
                    filled: true,
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0)
                      ),
                    ),
              ),
              /*new RaisedButton(
                  child: Text("Submit a Photo"),
                  onPressed: _imageSelectorGallery
              ),
              new Center(
                  child: _imageFile == null
                      ? new Text("Nothing here!")
                      : new Image.file(_imageFile)
              ),*/
              new Padding(padding: new EdgeInsets.only(top: 15.0),),
              new RaisedButton(
                child: new Text("Submit"),
                color: Color.fromRGBO(102, 0, 0, 50),
                onPressed: () async {
                  await _textMessageSubmitted(
                      _textEditingController.text, _imageFile);
                  _imageFile = null;
                  Navigator.pop(context);
                },
              )
            ],
          );
        }));
  }

  Future<Null> _textMessageSubmitted(
      String text, var imageFile) async {
    _textEditingController.clear();

    setState(() {
      _isComposingMessage = false;
    });

    await _ensureLoggedIn();

    if (imageFile == null) {
      _sendMessage(
          messageText: text,
          imageUrl: null,
          location: main.MapFunctionality().getInitialPlace.locality);
    } else {


      var timestamp = new DateTime.now().millisecondsSinceEpoch;
      StorageReference storageReference = FirebaseStorage.instance
          .ref()
          .child("img_" + timestamp.toString() + ".jpg");

      StorageUploadTask uploadTask = storageReference.putFile(imageFile);

      StorageTaskSnapshot storage = await uploadTask.onComplete;

      String downloadUrl = await storage.ref.getDownloadURL();

      _sendMessage(
          messageText: text,
          imageUrl: downloadUrl.toString(),
          location: main.MapFunctionality().getInitialPlace.locality);
    }
  }

  void _sendMessage({String messageText, String imageUrl, String location}) {
    reference.push().set({
      'text': messageText,
      'email': googleSignIn.currentUser.email,
      'imageUrl': imageUrl,
      'senderName': googleSignIn.currentUser.displayName,
      'senderPhotoUrl': googleSignIn.currentUser.photoUrl,
      'location': location,
      'upvotes': 0,
    });

    analytics.logEvent(name: 'send_message');
  }

  Future<Null> _ensureLoggedIn() async {
    GoogleSignInAccount signedInUser = googleSignIn.currentUser;

    if (signedInUser == null)
      signedInUser = await googleSignIn.signInSilently();

    if (signedInUser == null) {
      await googleSignIn.signIn();

      analytics.logLogin();
    }

    if (await auth.currentUser() == null) {
      GoogleSignInAuthentication credentials =
          await googleSignIn.currentUser.authentication;

      //await auth.signInWithGoogle(
      //  idToken: credentials.idToken, accessToken: credentials.accessToken);
    }
  }
}
