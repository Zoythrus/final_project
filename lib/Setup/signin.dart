import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:sightseer/secondmain.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart'  as http;
import 'package:sightseer/main.dart' as main;
import 'dart:convert' show json;

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

class LoginPage extends StatefulWidget {
  @override
  State createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String _email, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _auth = FirebaseAuth.instance;
  //final GoogleSignIn _googleSignIn = new GoogleSignIn();
  GoogleSignInAccount _currentUser;
  String _contactText;

  @override
  void initState(){
    super.initState();
      _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount  account) {
        setState((){
          _currentUser = account;
        });
        if (_currentUser != null){
          _handleGetContact();
        }
    });
      _googleSignIn.signInSilently();
  }

  Future<void> _handleGetContact()  async{
    setState(() {
      _contactText  =  "Loading contact info...";
    });
    final http.Response response = await http.get(
      'https://people.googleapis.com/v1/people/me/connections'
        '?requestMask.includeField=person.names',
      headers: await _currentUser.authHeaders,
    );
    if (response.statusCode !=  200){
      setState(() {
        _contactText = "People API gave a ${response.statusCode}  "
        "response. Check logs for details.";
      });
      print('People API ${response.statusCode} response: ${response.body}');
      return;
    }

    final Map<String, dynamic> data = json.decode(response.body);
    final String namedContact = _pickFirstNamedContact(data);
    setState(() {
      if(namedContact  != null){
        _contactText = "I see you know $namedContact!";
      } else{
        _contactText = "No contacts to display";
      }
    });
  }


  String _pickFirstNamedContact(Map<String, dynamic> data) {
    final List<dynamic> connections = data['connections'];
    final Map<String, dynamic> contact = connections?.firstWhere(
          (dynamic contact) => contact['names'] != null,
      orElse: () => null,
    );
    if (contact  != null){
      final Map<String, dynamic> name = contact['names'].firstWhere(
          (dynamic name) => name ['displayName'] != null,
        orElse: () => null,
      );
      if (name != null){
        return name['displayName'];
      }
    }
    return null;
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
      //_openMyPage(context);
      //Navigator.push(
        //  context, MaterialPageRoute(builder: (context) => SecondMain()));
    } catch (error) {
      print(error);
    }
  }

  Future<void> _handleSignOut() async {
    _googleSignIn.disconnect();
  }

  Widget _buildBody() {

    if (_currentUser != null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ListTile(
            leading: GoogleUserCircleAvatar(
              identity: _currentUser,
            ),
            title: Text(_currentUser.displayName ?? '', style: TextStyle(color: Color.fromRGBO(255, 253, 208, 100))),
            subtitle: Text(_currentUser.email ?? '', style: TextStyle(color: Color.fromRGBO(255, 253, 208, 100))),
          ),
          const Text("Signed in successfully.", style: TextStyle(color: Color.fromRGBO(255, 253, 208, 100))),
          //Text(_contactText ?? ''),
          RaisedButton(
            child: const Text('SIGN OUT'),
            onPressed: _handleSignOut,
          ),
          RaisedButton(
            child: const Text('Let\'s Move Forward'),
            onPressed: () => _openMyPage(context),
          ),

        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          const Text("You are not currently signed in.",style: TextStyle(color: Color.fromRGBO(255, 253, 208, 100))),
          RaisedButton(
            child: const Text('SIGN IN'),
            onPressed: _handleSignIn,
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(102, 0, 0, 25),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(255, 253, 208, 50),
          title: const Text('Google Sign In'),
        ),
        body: ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: _buildBody(),
        ));
  }
}

void _openMyPage(context){
  Navigator.push(context, MaterialPageRoute(builder: (context) => SecondMain()));
}

/**
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(255, 253, 208, 100),
          title: Text('Sign in'),
        ),
        body: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                validator: (input) {
                  if (input.isEmpty) {
                    return 'Please type in a Valid Email';
                  }
                },
                onSaved: (input) => _email = input,
                decoration: InputDecoration(labelText: 'Email'),
              ),
              TextFormField(
                validator: (input) {
                  if (input.length < 6) {
                    return 'Your Password must be at least 6 characters';
                  }
                },
                onSaved: (input) => _password = input,
                decoration: InputDecoration(labelText: 'Password'),
                obscureText: true,
              ),
              RaisedButton(
                onPressed: signIn,
                child: Text('Sign in'),
              ),
            ],
          ),
        ),
    );
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  Future<void> signIn() async {
    final formState = _formKey.currentState;
    if (formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: _email, password: _password);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SecondMain()));
      } catch (e) {
        print(e.message);
      }
    }
  }
}
*/
