import 'package:flutter/material.dart';
import 'package:sightseer/chatscreen.dart';
import 'package:sightseer/main.dart' as main;

class ChatPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(255, 253, 208, 100),
        title: new Text("Live Chat Near You In " + main.MapFunctionality().getInitialPlace.locality),
      ),
      body: new ChatScreen()
    );
  }
}